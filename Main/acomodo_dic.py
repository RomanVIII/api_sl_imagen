def limpiar(doc, fields):
    dic = {}
    if "Nombre" in fields:
        if doc[fields.index('Nombre')]:
            dic["name"] = doc[fields.index('Nombre')]
    if "Apellido Paterno" in fields:
        if doc[fields.index('Apellido Paterno')]:
            dic["lname"] = doc[fields.index('Apellido Paterno')]
    if "Apellido Materno" in fields:
        if doc[fields.index('Apellido Materno')]:
            dic["slname"] = doc[fields.index('Apellido Materno')]
    if "Genero" in fields:
        if doc[fields.index('Genero')]:
            dic["sexo"] = doc[fields.index('Genero')]
    if "Fecha de nacimiento" in fields:
        if doc[fields.index('Fecha de nacimiento')]:
            dic["day"], dic["month"], dic["year"] = doc[fields.index('Fecha de nacimiento')].split("/")
    if "Estado de nacimiento" in fields:
        if doc[fields.index('Estado de nacimiento')]:
            dic["edo"] = doc[fields.index('Estado de nacimiento')]
    if "Email" in fields:
        if doc[fields.index('Email')]:
            dic["mail_sujeto"] = doc[fields.index('Email')]
    if "Numero celular" in fields:
        if doc[fields.index('Numero celular')]:
            dic["num_cel"] = doc[fields.index('Numero celular')]
    return dic