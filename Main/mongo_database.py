import pymongo

def Mongoconexion(database):
    MONGODB_HOST = '' #direccion de la base de datos ej: "0.0.0.0"
    MONGODB_PORT = '' #puerto que esta abierto para la conexion a la base de datos, usualmente 27017
    MONGODB_TIMEOUT = 1000 #tiempo en ms que el cleinte esperara la respuesta de la base de datos
    MONGODB_DATABASE = database #nombre de la base de datos
    MONGODB_USER = '' #usuario d ela base de datos
    MONGODB_PASS = '' #contraseña de la base de datos
    URI_CONNECTION = 'mongodb://' + MONGODB_USER + ':' + MONGODB_PASS + '@' + MONGODB_HOST + ':' + MONGODB_PORT + '/admin'
    client = pymongo.MongoClient(URI_CONNECTION, connectTimeoutMS=MONGODB_TIMEOUT)
    return client, MONGODB_DATABASE


def url_api(nombre, apellido_p, apellido_m, dia_nacimiento, mes_nacimiento, anio_nacimiento, sexo, estado_nacimiento,
            correo_sujeto, numero_sujeto, usuario, password, cupon):
    url_api = "http://104.225.140.130:5006/api_imagen?" + "nombre=" + nombre + "&apellido_p=" + apellido_p + \
              "&apellido_m=" + apellido_m + "&dia_nacimiento=" + dia_nacimiento + "&mes_nacimiento=" + \
              mes_nacimiento + "&anio_nacimiento=" + anio_nacimiento + "&sexo=" + sexo + "&estado_nacimiento=" + \
              str(estado_nacimiento) + "&correo_sujeto=" + correo_sujeto + "&numero_sujeto=" + str(numero_sujeto) + \
              "&usuario_email=" + usuario + "&usuario=" + usuario + "&password=" + password + "&cupon=" + cupon
    return url_api

if __name__ == '__main__':
    Mongoconexion('Mini_Base_Central')