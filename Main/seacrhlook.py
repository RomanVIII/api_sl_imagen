import pandas as pd
import requests
import zipfile
import io
import acomodo_dic
import getpass
from mongo_database import url_api
from sys import argv, path
path.append('../Base_SL')

def lectura_tabla(Tabla, usuario, password, cupon):
    fields = list(Tabla.columns)
    rows = Tabla.values.tolist()
    limpios = [acomodo_dic.limpiar(row, fields) for row in rows]
    use_api(limpios, usuario, password, cupon)

def use_api(limpios, usuario, password, cupon):
    for limpio in limpios:
        nombre = limpio["name"]
        apellido_p = limpio["lname"]
        apellido_m = limpio["slname"]
        dia_nacimiento = limpio["day"]
        mes_nacimiento = limpio["month"]
        anio_nacimiento = limpio["year"]
        sexo = limpio["sexo"]
        estado_nacimiento = limpio["edo"]
        correo_sujeto = limpio["mail_sujeto"]
        numero_sujeto = limpio["num_cel"]
        url_peticion = url_api(nombre, apellido_p, apellido_m, dia_nacimiento, mes_nacimiento, anio_nacimiento, sexo,
                               estado_nacimiento, correo_sujeto, numero_sujeto, usuario, password, cupon)
        respuesta_api = requests.get(url=url_peticion, stream=True)
        save_path = nombre + apellido_p + apellido_m + str(estado_nacimiento) + correo_sujeto + ".jpg"
        zipdata = zipfile.ZipFile(io.BytesIO(respuesta_api.content))
        zips_content = zipdata.infolist()
        for zip_content in zips_content:
            zip_content.filename = save_path
            zipdata.extract(zip_content)


def read_xlmx(name_arch, usuario, password, cupon):
    relative_path = "../Base_SL/"+name_arch
    Tabla = pd.read_excel(relative_path, encoding="utf8", errors='ignore')
    lectura_tabla(Tabla, usuario, password, cupon)
    return "se leyó xlsx"

def read_csv(name_arch, usuario, password, cupon):
    relative_path = "../Base_SL/" + name_arch
    Tabla = pd.read_csv(relative_path, encoding="utf8", squeeze=True)
    lectura_tabla(Tabla, usuario, password, cupon)
    return "se leyó csv"

def read_mongo(usuario, password, cupon):
    from mongo_database import Mongoconexion
    database = "" #colocar el nombre de la base de datos
    cliente, base = Mongoconexion(database)
    coleccion = cliente[base][database]
    tabla = [x for x in coleccion.find()]
    use_api(tabla, usuario, password, cupon)
    return "se leyó mongo"


if __name__ == "__main__":
    correo_soporte = "contacto@searchlook.mx"
    metodo = argv[1]
    mensaje = "Aun no tenemos soporte para esa base, por favor contacte con " + correo_soporte + \
              ", correo de soporte de Searchlook"
    usuario = input("Usuario: ")
    password = getpass.getpass("contraseña: ")
    cupon = input("cupon: CB/SB ")
    if metodo == "mongo":
        mensaje = read_mongo(usuario, password, cupon)
    else:
        name_arch = argv[2]
        if metodo == "xlsx":
            mensaje = read_xlmx(name_arch, usuario, password, cupon)
        elif metodo == "csv":
            mensaje = read_csv(name_arch, usuario, password, cupon)
    print(mensaje)